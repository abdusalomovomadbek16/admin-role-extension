class Administrator:
    """
    Represents an administrator with authorization and database management capabilities.
    """

    def __init__(self, database):
        """
        Initializes an instance of the Administrator class.
        """
        self.database = database

    def authorize(self):
        """
        Authorizes the administrator by prompting for a username and password.

        The function compares the entered credentials with the correct credentials
        to determine if the administrator is authorized.
        """
        admin_username = "admin"
        admin_password = "password"

        entered_username = input("Enter the username: ")
        entered_password = input("Enter the password: ")

        if entered_username == admin_username and entered_password == admin_password:
            print("Authorization successful!")
            # Proceed with the administrator tasks
        else:
            print("Authorization failed! Access denied.")
            # Handle unsuccessful authorization

    def add_item(self):
        """
        Adds a new item to the database.

        The function prompts for item details such as name, price, and category,
        and updates the database with the new item.
        """
        item_name = input("Enter the name of the item: ")
        item_price = float(input("Enter the price of the item: "))
        item_category = input("Enter the category of the item: ")

        self.database.add_item(item_name, item_price, item_category)

    def remove_item(self):
        """
        Removes an item from the database.

        The function displays a list of items with their IDs, prompts for the ID of the item to be removed,
        and updates the database by removing the corresponding item.
        """
        items = self.database.get_items()

        if not items:
            print("No items in the database.")
            return

        print("The following items are available:")
        for item_id, item_name in items.items():
            print(f"ID: {item_id}, Name: {item_name}")

        item_id = input("Enter the ID of the item to remove: ")

        self.database.remove_item(item_id)

    def add_category(self):
        """
        Adds a new category to the database.

        The function prompts for the name of the new category
        and updates the database with the new category.
        """
        category_name = input("Enter the name of the new category: ")

        self.database.add_category(category_name)