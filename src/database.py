class Database:
    def __init__(self):
        # Initialize the empty list to store items
        self.items = []

    def add_item(self, item_name, item_price, item_category):
        # Create a new item dictionary
        item = {
            "name": item_name,
            "price": item_price,
            "category": item_category
        }
        # Add the item to the list
        self.items.append(item)
        print("Item added successfully!")

    def remove_item(self, item_id):
        for item in self.items:
            if item_id == str(id(item)):
                self.items.remove(item)
                print("Item removed successfully!")
                return
        print("Item not found!")

    def get_items(self):
        items_dict = {}
        for item in self.items:
            item_id = str(id(item))
            items_dict[item_id] = item["name"]
        return items_dict

    def add_category(self, category_name):
        # Implement the logic to add the category to the database
        print(f"Category '{category_name}' added successfully!")